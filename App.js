import React, {Component} from 'react';
import { 
  StyleSheet,
  Text,
  View,
  YellowBox,
} from 'react-native';

import ImageBrowser from 'react-native-interactive-image-gallery';

YellowBox.ignoreWarnings(['']);

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      imageURLs: [
        {
          id: 1,
          title: "Image 1",
          description: "This is sample image 1",
          thumbnail: "https://images.unsplash.com/photo-1537914675540-ec9f82fbd752?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8f577e49d7737b80e76e2715fdfb1a93&auto=format&fit=crop&w=1316&q=80",
          URI: "https://images.unsplash.com/photo-1537914675540-ec9f82fbd752?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8f577e49d7737b80e76e2715fdfb1a93&auto=format&fit=crop&w=1316&q=80",
        },
        {
          id: 2,
          title: "Image 2",
          description: "This is sample image 2",
          thumbnail: "https://images.unsplash.com/photo-1537895445884-adb79d9f913f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8276915f24980a4b7ee32da1988f82b6&auto=format&fit=crop&w=667&q=80",
          URI: "https://images.unsplash.com/photo-1537895445884-adb79d9f913f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8276915f24980a4b7ee32da1988f82b6&auto=format&fit=crop&w=667&q=80",
        },
        {
          id: 3,
          title: "Image 3",
          description: "This is sample image 3",
          thumbnail: "https://images.unsplash.com/photo-1537945236368-bdca7add3d47?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=1887bb73cc066a2609057524220e1cc5&auto=format&fit=crop&w=751&q=80",
          URI: "https://images.unsplash.com/photo-1537945236368-bdca7add3d47?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=1887bb73cc066a2609057524220e1cc5&auto=format&fit=crop&w=751&q=80",
        },
        {
          id: 4,
          title: "Image 4",
          description: "This is sample image 4",
          thumbnail: "https://images.unsplash.com/photo-1537964769190-561c0c37b043?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e22528357efc521f19fff44d22da7fc7&auto=format&fit=crop&w=334&q=80",
          URI: "https://images.unsplash.com/photo-1537964769190-561c0c37b043?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e22528357efc521f19fff44d22da7fc7&auto=format&fit=crop&w=334&q=80",
        },
        {
          id: 5,
          title: "Image 5",
          description: "This is sample image 5",
          thumbnail: "https://images.unsplash.com/photo-1537923814349-0c14f54677f0?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d3116ef300564ce437a7b65e34e820f8&auto=format&fit=crop&w=500&q=60",
          URI: "https://images.unsplash.com/photo-1537923814349-0c14f54677f0?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d3116ef300564ce437a7b65e34e820f8&auto=format&fit=crop&w=500&q=60",
        },
        {
          id: 6,
          title: "Image 6",
          description: "This is sample image 6",
          thumbnail: "https://images.unsplash.com/photo-1537815091908-0017f9b4ecc4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5cc8457b0b240afe2be3a4d44c356516&auto=format&fit=crop&w=500&q=60",
          URI: "https://images.unsplash.com/photo-1537815091908-0017f9b4ecc4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5cc8457b0b240afe2be3a4d44c356516&auto=format&fit=crop&w=500&q=60",
        },
        {
          id: 7,
          title: "Image 7",
          description: "This is sample image 7",
          thumbnail: "https://images.unsplash.com/photo-1537949009321-b84cfba79450?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b0ded6ae6582e4623f19a7aebc6b25cc&auto=format&fit=crop&w=500&q=60",
          URI: "https://images.unsplash.com/photo-1537949009321-b84cfba79450?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b0ded6ae6582e4623f19a7aebc6b25cc&auto=format&fit=crop&w=500&q=60",
        },
        {
          id: 8,
          title: "Image 8",
          description: "This is sample image 8",
          thumbnail: "https://images.unsplash.com/photo-1537904408606-5860d995c819?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=0f33c13b8fa298d2106f4a9ce213b8f2&auto=format&fit=crop&w=500&q=60",
          URI: "https://images.unsplash.com/photo-1537904408606-5860d995c819?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=0f33c13b8fa298d2106f4a9ce213b8f2&auto=format&fit=crop&w=500&q=60",
        },
        {
          id: 9,
          title: "Image 9",
          description: "This is sample image 9",
          thumbnail: "https://images.unsplash.com/photo-1537956900246-de817d5af91d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=00d50c9bb8c3cd4e3be9b6a105802e56&auto=format&fit=crop&w=500&q=60",
          URI: "https://images.unsplash.com/photo-1537956900246-de817d5af91d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=00d50c9bb8c3cd4e3be9b6a105802e56&auto=format&fit=crop&w=500&q=60",
        },

      ]
    };
  }
  render() {
    return <ImageBrowser images={this.state.imageURLs} />
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
